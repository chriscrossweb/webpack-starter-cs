const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require('webpack');
const path = require("path");
const bootstrapEntryPoints = require('./webpack.bootstrap.config');
const glob = require('glob');
const PurifyCSSPlugin = require('purifycss-webpack');

const isProd = process.env.NODE_ENV === 'production'; //return true or false

console.log('enviroment', process.env.NODE_ENV);
console.log('isProd', isProd);

const cssDev = [
	'style-loader',
	'css-loader?sourceMap',
	'sass-loader',
	{
		loader: 'sass-resources-loader',
		options: {
			// Provide path to the file with resources
			resources: [
				'./src/styles/app.scss'
			],
		},
	}];

const cssProd = ExtractTextPlugin.extract({
	fallback: 'style-loader',
	use: ['css-loader','sass-loader', {
		loader: 'sass-resources-loader',
		options: {
			// Provide path to the file with resources
			resources: [
				'./src/styles/app.scss'
			],
		},
	}],
	publicPath: '/dist'
})
const cssConfig = isProd ? cssProd : cssDev;

const cssOnly = ExtractTextPlugin.extract( {
	fallback: 'style-loader',
	use: [
		{
			loader: 'css-loader',
			options: {
				sourceMap: true
			}
		}
	],
	publicPath: '/dist'
});


const bootstrapConfig = isProd ? bootstrapEntryPoints.prod : bootstrapEntryPoints.dev;

let srcPath = path.resolve(__dirname, 'src');

module.exports = {
	entry: {
		app: './src/script/app.js',
		contact: srcPath + '/script/contact.js',
		//bootstrap: bootstrapConfig,
		vendor: [
		 	bootstrapConfig
		]
	},
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: '[name].bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use : cssOnly
			},
			{
				test: /\.scss$/, //als je /\.s?css$/, doet krijg ik dubbele css code in de production compilatie
				use: cssConfig
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: 'babel-loader'
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				use : [
					{
						loader: 'file-loader',
						options: {
							// name: '[name].[ext]', //default is '[path][name].[ext]' without a outputPath
							// outputPath: 'images/',
							// publicPath: 'images' //de path in de html
							name: '[path][name].[ext]' //default behaviour
						}
					},
					{
						loader: 'image-webpack-loader',
						options: {  //documentation: https://www.npmjs.com/package/image-webpack-loader
							bypassOnDebug : true,
							optipng: {
								optimizationLevel: 7
							},
							pngquant: {
								quality: '65-90',
								speed: 4
							}
						}
					}
				]
			},
			{ test: /\.(woff2?)$/, use: 'url-loader?limit=10000&name=fonts/[name].[ext]' },
			{ test: /\.(ttf|eot)$/, use: 'file-loader?name=fonts/[name].[ext]' },

			// // Bootstrap 3
			// { test:/bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/, use: 'imports-loader?jQuery=jquery' },

			//Bootstrap 4
			{ test: /bootstrap[\/\\]dist[\/\\]js[\/\\]umd[\/\\]/, loader: 'imports-loader?jQuery=jquery' },
			//{ test: /bootstrap\/dist\/js\/umd\//, loader: 'imports-loader?jQuery=jquery' },
		]
	},
	devServer: {
		contentBase: path.join(__dirname, "dist"),
		compress: true,
		hot: true,
		open: true,
		stats: 'errors-only'
	},
	plugins: [

		new webpack.DefinePlugin({ 'NODE_ENV': JSON.stringify(process.env.NODE_ENV) }),

		new HtmlWebpackPlugin( {
			title: 'Hello World',
			minify: {
				collapseWhitespace: true
			},
			filename: 'index.html', //you can point it also to a file outsite the diff
			excludeChunks: ['contact'],
			hash: true, //generate a unique id (time stamp) to load the css (prevent caching)
			template: './src/index.html'
		}),

		new HtmlWebpackPlugin( {
			title: 'Contact pagina',
			chunks :'[contact]', // met chunks kan je aangeven alleen includeren bijv. contact.js
			filename: 'contact.html', //you can point it also to a file outsite the diff
			hash: false, //generate a unique id (time stamp) to load the css (prevent caching)
			template: './src/contact.html'
		}),

		new ExtractTextPlugin({
			filename: '/css/[name].css', //kan ook hard een filename worden gezet bijv. "styles.css"
			disable: !isProd,
			allChunks: true
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
		// Make sure this is after ExtractTextPlugin!
		new PurifyCSSPlugin({
			// Give paths to parse for rules. These should be absolute!
			paths: glob.sync(path.join(__dirname, 'src/*.html'))
		})
	]
}